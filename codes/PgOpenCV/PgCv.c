#include "CvWrapper.h"
#include <stdio.h>
#include "postgres.h"
#include "fmgr.h"

PG_MODULE_MAGIC;



PG_FUNCTION_INFO_V1(PgCv);


Datum PgCv(PG_FUNCTION_ARGS) {
  
  int reponse;
  reponse=Cv_W();
  PG_RETURN_INT32(reponse);
       
}
