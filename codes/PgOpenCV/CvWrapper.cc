//! [includes]
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <unistd.h>
#include <iostream>
#include "CvWrapper.h"

using namespace cv;
//! [includes]

extern "C" {
  
  int Cv_W(){
     //! [imread]
    std::string image_path = samples::findFile("/home/dac/Bureau/OpenGeo/blog/codes/PgOpenCV/sample3.png");
    Mat img = imread(image_path, IMREAD_GRAYSCALE);
    //! [imread]

    //! [empty]
    if(img.empty())
    {
        std::cout << "Could not read the image: " << image_path << std::endl;
        return 1;
    }
    //! [empty]
    
    std::cout << "Could read the image: " << image_path << std::endl;
    //! [imshow]
    imshow("Display window", img);
  
    //! [imshow]
    int k = waitKey(0); // Wait for a keystroke in the window
    //! [imshow]

    //! [release memory on keystroke]
    if(k)
    {
      img.release() ;
    }
    //! [release]

    return 0;

  }
}
