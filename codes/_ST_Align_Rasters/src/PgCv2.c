#include <postgres.h>
#include <fmgr.h>
#include <funcapi.h> /* for SRF */
#include <utils/builtins.h> /* for text_to_cstring() */
#include <access/htup_details.h> /* for heap_form_tuple() */
#include <utils/lsyscache.h> /* for get_typlenbyvalalign */
#include <utils/array.h> /* for ArrayType */
#include <utils/guc.h> /* for ArrayType */
#include <catalog/pg_type.h> /* for INT2OID, INT4OID, FLOAT4OID, FLOAT8OID and TEXTOID */
#include <utils/memutils.h> /* For TopMemoryContext */
#include "../../postgis_config.h"
#include "rtpostgis.h"
#include "rtpg_internal.h"
#include "stringbuffer.h"

//! [OpenCV includes]
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <unistd.h>
#include <iostream>
//! [includes]

using namespace cv;

PG_FUNCTION_INFO_V1(RASTER_asCV);
Datum RASTER_asCV(PG_FUNCTION_ARGS)
{
  rt_pgraster *pgraster = NULL;
  rt_raster raster;
  rt_band band = NULL; /* Pointer to rt_band_t */
  uint32_t numBands;
  void* pband=NULL;
  rt_pixtype px_type;
  uint8_t type;
    
  /* pgraster is null, return null */
  if (PG_ARGISNULL(0)) PG_RETURN_NULL();
  /* Get serialised raster by name*/
  pgraster = (rt_pgraster *) PG_DETOAST_DATUM(PG_GETARG_DATUM(0));
  /* Deserialise pgraster into raster */
  raster = rt_raster_deserialize(pgraster, FALSE);
  if (!raster) {
    PG_FREE_IF_COPY(pgraster, 0);
    elog(ERROR, "RASTER_asCV: Could not deserialize raster");
    PG_RETURN_NULL();
	}
  /* Getting the band:
     uint16_t
     rt_raster_get_num_bands(rt_raster raster) {


     assert(NULL != raster);

     return raster->numBands;
     }
  */
  numBands = rt_raster_get_num_bands(raster);

    
  if(numbands! = 1)
    {
       PG_FREE_IF_COPY(pgraster, 0);
       rt_raster_destroy(raster);
       elog(ERROR, "RASTER_asCV: One band raster required");
       PG_RETURN_NULL();
    }

  band = rt_raster_get_band(raster,0);

    /* rt_band
       rt_raster_get_band(rt_raster raster, int n) {
       assert(NULL != raster);

       if (n >= raster->numBands || n < 0)
       return NULL;

       return raster->bands[n];
       } */
  
  px_type = band->pixtype;
  int pixbytes = rt_pixtype_size(px_type);
  pband = band->data.mem; /* Pointer to the band, to be transfered to OpenCV */

  /* Second band */
  
  pgraster = (rt_pgraster *) PG_DETOAST_DATUM(PG_GETARG_DATUM(1));
  /* Deserialise pgraster into raster */
  rt_raster To_rast = rt_raster_deserialize(pgraster, FALSE);
  if (!To_rast) {
    PG_FREE_IF_COPY(pgraster, 0);
    elog(ERROR, "RASTER_asCV: Could not deserialize raster");
    PG_RETURN_NULL();
	}
 
  numBands = rt_raster_get_num_bands(To_rast);
  if(numbands! = 1)
    {
       PG_FREE_IF_COPY(pgraster, 0);
       rt_raster_destroy(To_rast);
       elog(ERROR, "RASTER_asCV: One band raster required");
       PG_RETURN_NULL();
    }

  rt_band To_band = rt_raster_get_band(To_rast,0);
  rt_pixtype To_px_type = To_band->pixtype;
  int To_pixbytes = rt_pixtype_size(To_px_type);
  void* To_pband = To_band->data.mem; /* Pointer to the band, to be transfered to OpenCV */





  
  /*For this simple example, images have CV_8U pixel's type, which is defined as 0 */
  type=CV_8UC1;

  /* Now we can pass pband and type  to openCV's wrapped mat constructor */

  void* img=wMat(raster->height,raster->width,type,pband,raster->width * pixbytes);

  
  
}
