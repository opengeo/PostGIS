#include "../include/imreg_fmt/image_registration.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include "Wrapper_reg.hpp"

int  main(int argc, char **argv)
{
    cv::Mat im0, im1;
    uchar* data_reg;
    int height,width,rowbytes;
    void* pband;
    void* To_pband;
    
    std::string image_path = cv::samples::findFile("sample1.png");
    im0 = cv::imread(image_path, cv::IMREAD_GRAYSCALE);
    image_path = cv::samples::findFile("sample3.png");
    im1 = cv::imread(image_path, cv::IMREAD_GRAYSCALE);
    height=im0.rows;
    width=im0.cols;
    rowbytes= width * 1;
    pband=(void *)im0.data;
    To_pband=(void*)im1.data;
    data_reg=ST_register(height,width,/*cv::CV_8UC1*/0,pband,To_pband,rowbytes);
    if (data_reg){
      printf("Success");
    }
    return 0;
}
