#include "../include/imreg_fmt/image_registration.h"
#include "Wrapper_reg.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>



extern "C" {
  
  /* 4 first pointed bytes will store the size on the data */
  
    uchar*  ST_register(int height,int width,char type, void* pband,void* To_pband,int rowbytes){
    
    /* Only the constructors change */
    cv::Mat im0= cv::Mat(height,width,type,pband, rowbytes);
    cv::Mat im1= cv::Mat(height,width,type,To_pband, rowbytes);
    /*DEBUG
    cv::imshow("im0", im0);
    cv::imshow("im1", im1);
    cv::waitKey(0);
    DEBUG*/
    ImageRegistration image_registration(im0);
    std::cout << "Registered OK ";
    // x, y, rotation, scale
    std::vector<double> transform_params(4, 0.0);
    cv::Mat registered_image;
    image_registration.registerImage(im1, registered_image, transform_params, true);


    std::cout << "x: " << transform_params[0] << ", y: "
              << transform_params[1] << ", rotation: " << transform_params[2]
              << ", scale: " << transform_params[3] << std::endl;

   
    
    
    return registered_image.data;
}



  };

  






