g++ -c -fPIC main_wrapper.cpp `pkg-config opencv4 --cflags --libs` -o main_wrapper.o
g++ -c -fPIC Wrapper_reg.cpp `pkg-config opencv4 --cflags --libs` -o Wrapper_reg.o
g++ -c -fPIC image_dft.cpp `pkg-config opencv4 --cflags --libs` -o image_dft.o

g++ -c -fPIC image_transforms.cpp `pkg-config opencv4 --cflags --libs` -o image_transforms.o

g++ -c -fPIC image_registration.cpp `pkg-config opencv4 --cflags --libs` -o image_registration.o


g++ main_wrapper.o -lCvWrapper `pkg-config opencv4 --cflags --libs` -L. -Wl,-rpath=. -lfftw3 -o main_wrapper_from_lib





g++ -shared -o libCvWrapper.so Wrapper_reg.o image_dft.o image_transforms.o image_registration.o `pkg-config opencv4 --cflags --libs`
