/* Merci à:
http://blog.eikke.com/index.php/ikke/2005/11/03/using_c_classes_in_c.html
et 
https://www.teddy.ch/c++_library_in_c/
*/

#include "MyClass.h"
#include "MyWrapper.h"

extern "C" {
        Interface *newMyClass() {
	  MyClass* x=new MyClass();
	  return (Interface *)x;
        }

        void MyClass_int_set(Interface *v, int i) {
	  /* Attention à l'erreur naïve suivante: caster v ne crée pas l'objet!!
	     (MyClass*)v->int_set(i);
	  Il faut déclarer et créer un objet intermédiaire, x par exemple:
	  Ne peut-on pas éviter toutes ces copies??*/
	  MyClass* x=(MyClass*)v;
	  x->int_set(i);
	  
	}

        int MyClass_int_get(Interface *v) {
	   MyClass* x=(MyClass*)v;
	   return x->int_get();
        }

        void deleteMyClass(Interface *v) {
	  MyClass* x=(MyClass*)v;
                delete x;
        }
}
