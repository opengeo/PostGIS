#ifndef __MYWRAPPER_H
#define __MYWRAPPER_H

typedef void Interface;

#ifdef __cplusplus
extern "C" {
#endif


  

Interface *newMyClass();

void MyClass_int_set(Interface *v, int i);

int MyClass_int_get(Interface *v);

void deleteMyClass(Interface *v);

#ifdef __cplusplus
}
#endif
#endif
